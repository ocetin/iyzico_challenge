import axios from "axios";
import {
  ROOT_URL,
  GET_ALL_EPISODES,
  GET_ALL_CHARACTERS,
  GET_SELECTED_CHARACTER_EPISODES
} from "./actionTypes";

export const getAllEpisodes = (page = 1) => {
  return dispatch => {
    dispatch({ type: GET_ALL_EPISODES.REQUEST });
    axios
      .get(`${ROOT_URL}/episode?page=${page}`)
      .then(res => {
        dispatch({ type: GET_ALL_EPISODES.SUCCESS, payload: res.data });
      })
      .catch(err => {
        dispatch({ type: GET_ALL_EPISODES.FAIL, payload: err });
      });
  };
};

export const getNextEpisodes = url => {
  return dispatch => {
    dispatch({ type: GET_ALL_EPISODES.REQUEST });
    axios
      .get(url)
      .then(res => {
        dispatch({ type: GET_ALL_EPISODES.SUCCESS, payload: res.data });
      })
      .catch(err => {
        dispatch({ type: GET_ALL_EPISODES.FAIL, payload: err });
      });
  };
};

export const getAllCharacters = (page = 1) => {
  return dispatch => {
    dispatch({ type: GET_ALL_CHARACTERS.REQUEST });
    axios
      .get(`${ROOT_URL}/character?page=${page}`)
      .then(res => {
        dispatch({
          type: GET_ALL_CHARACTERS.SUCCESS,
          payload: res.data
        });
      })
      .catch(err => {
        dispatch({ type: GET_ALL_CHARACTERS.FAIL, payload: err });
      });
  };
};

export const getNextCharacters = url => {
  return dispatch => {
    dispatch({ type: GET_ALL_CHARACTERS.REQUEST });
    axios
      .get(url)
      .then(res => {
        dispatch({
          type: GET_ALL_CHARACTERS.SUCCESS,
          payload: res.data
        });
      })
      .catch(err => {
        dispatch({ type: GET_ALL_CHARACTERS.FAIL, payload: err });
      });
  };
};

export const getSelectedCharacterEpisodes = episodes => {
  return dispatch => {
    dispatch({ type: GET_SELECTED_CHARACTER_EPISODES.REQUEST });
    axios
      .get(`${ROOT_URL}/episode/[${episodes}]`)
      .then(res => {
        dispatch({
          type: GET_SELECTED_CHARACTER_EPISODES.SUCCESS,
          payload: res.data
        });
      })
      .catch(err => {
        dispatch({ type: GET_SELECTED_CHARACTER_EPISODES.FAIL, payload: err });
      });
  };
};
