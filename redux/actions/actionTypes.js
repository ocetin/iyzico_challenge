const asyncActionType = type => ({
  REQUEST: `${type}_REQUEST`,
  SUCCESS: `${type}_SUCCESS`,
  FAIL: `${type}_FAIL`
});

export const ROOT_URL = "https://rickandmortyapi.com/api";

export const GET_ALL_EPISODES = asyncActionType("GET_ALL_EPISODES");
export const GET_ALL_CHARACTERS = asyncActionType("GET_ALL_CHARACTERS");
export const GET_SELECTED_CHARACTER_EPISODES = asyncActionType(
  "GET_SELECTED_CHARACTER_EPISODES"
);
