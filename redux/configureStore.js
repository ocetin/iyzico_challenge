import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers/rootReducer";
import thunk from "redux-thunk";

const middleware = [thunk].filter(Boolean);

export default function configureStore() {
  const store = createStore(rootReducer, applyMiddleware(thunk));

  return store;
}
