import { combineReducers } from "redux";
import rickAndMort from "./rickAndMortReducer";

export default combineReducers({ rickAndMort: rickAndMort });
