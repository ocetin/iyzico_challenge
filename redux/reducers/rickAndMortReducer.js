import {
  GET_ALL_CHARACTERS,
  GET_ALL_EPISODES,
  GET_SELECTED_CHARACTER_EPISODES
} from "../actions/actionTypes";

const INITIAL_STATE = { episodes: [], characters: [], characterEpisodes: [] };

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_ALL_EPISODES.REQUEST:
      return {
        ...state,
        getAllEpisodesLoading: true,
        getAllEpisodesFailed: false,
        getAllEpisodesSuccess: false,
        getAllEpisodesError: ""
      };

    case GET_ALL_EPISODES.SUCCESS:
      return {
        ...state,
        nextEpisodesUrl: action.payload.info.next,
        episodes: [...state.episodes, ...action.payload.results],
        getAllEpisodesLoading: false,
        getAllEpisodesFailed: false,
        getAllEpisodesSuccess: true,
        getAllEpisodesError: ""
      };

    case GET_ALL_EPISODES.FAIL:
      return {
        ...state,
        getAllEpisodesLoading: false,
        getAllEpisodesFailed: false,
        getAllEpisodesSuccess: true,
        getAllEpisodesError: action.payload.error
      };

    case GET_ALL_CHARACTERS.REQUEST:
      return {
        ...state,
        getAllCharactersLoading: true,
        getAllCharactersFailed: false,
        getAllCharactersSuccess: false,
        getAllCharactersError: ""
      };

    case GET_ALL_CHARACTERS.SUCCESS:
      return {
        ...state,
        nextCharactersUrl: action.payload.info.next,
        characters: [...state.characters, ...action.payload.results],
        getAllCharactersLoading: false,
        getAllCharactersFailed: false,
        getAllCharactersSuccess: true,
        getAllCharactersError: ""
      };

    case GET_ALL_CHARACTERS.FAIL:
      return {
        ...state,
        getAllCharactersLoading: false,
        getAllCharactersFailed: false,
        getAllCharactersSuccess: true,
        getAllCharactersError: action.payload.error
      };

    case GET_SELECTED_CHARACTER_EPISODES.REQUEST:
      return {
        ...state,
        characterEpisodes: [],
        getCharacterEpisodesLoading: true,
        getCharacterEpisodesFailed: false,
        getCharacterEpisodesSuccess: false,
        getCharacterEpisodesError: ""
      };

    case GET_SELECTED_CHARACTER_EPISODES.SUCCESS:
      return {
        ...state,
        characterEpisodes: action.payload,
        getCharacterEpisodesLoading: false,
        getCharacterEpisodesFailed: false,
        getCharacterEpisodesSuccess: true,
        getCharacterEpisodesError: ""
      };

    case GET_SELECTED_CHARACTER_EPISODES.FAIL:
      return {
        ...state,
        getCharacterEpisodesLoading: false,
        getCharacterEpisodesFailed: false,
        getCharacterEpisodesSuccess: true,
        getCharacterEpisodesError: action.payload.error
      };
    default:
      return { ...state };
  }
};
