import React, { Component } from "react";
import { View, StyleSheet, Text, Image } from "react-native";
class CharacterDetailItem extends Component {
  render() {
    const { characterEpisodes } = this.props;
    const { image, name, origin } = this.props.character;

    return (
      <View style={style.container}>
        <Image
          id="character-image"
          style={style.characterImage}
          source={{ uri: image }}
        />
        <Text id="character-name">{`Name: ${name}`}</Text>
        <Text id="character-origin">{`Origin: ${origin.name}`}</Text>
        <Text id="character-episode">The Last Episodes</Text>
        {characterEpisodes.map(episode => {
          return (
            <Text id={episode.id} key={episode.id}>
              {episode.name}
            </Text>
          );
        })}
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { alignItems: "center" },
  characterImage: {
    width: 300,
    height: 300,
    alignSelf: "center",
    marginBottom: 20
  }
});

export default CharacterDetailItem;
