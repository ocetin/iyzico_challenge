import React, { Component } from "react";
import { StyleSheet, View, Text } from "react-native";

class EpisodeItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { name, air_date, episode } = this.props.item;
    return (
      <View style={style.container}>
        <Text id="episode">{`Episode: ${episode}`}</Text>
        <Text id="episode-name">{`Name: ${name}`}</Text>
        <Text id="air-date">{`Air date: ${air_date}`}</Text>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    height: 150,
    backgroundColor: "white",
    justifyContent: "space-between",
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10
  }
});

export default EpisodeItem;
