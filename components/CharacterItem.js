import React, { Component } from "react";
import { StyleSheet, View, Image, Text, TouchableOpacity } from "react-native";

class CharacterItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSelect() {
    this.props.handleSelect();
  }

  render() {
    const { image, name } = this.props.item;
    return (
      <TouchableOpacity onPress={this.handleSelect.bind(this)}>
        <View style={style.characterItemContainer}>
          <Image source={{ uri: image }} style={style.characterImage} />
          <Text style={style.characterName}>{name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const style = StyleSheet.create({
  characterItemContainer: {
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "white",
    justifyContent: "space-between",
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10
  },
  characterImage: {
    width: 100,
    height: 100
  },
  characterName: { flex: 1, textAlign: "center" }
});

export default CharacterItem;
