import React from "react";
import { Image } from "react-native";
import {
  createBottomTabNavigator,
  createStackNavigator
} from "react-navigation";
import Characters from "./containers/Characters/Characters";
import Episodes from "./containers/Episodes/Episodes";
import CharacterDetail from "./containers/Characters/CharacterDetail";

const CharacterStack = createStackNavigator({
  Characters: { screen: Characters },
  CharacterDetail: { screen: CharacterDetail }
});

const EpisodesStack = createStackNavigator({
  Episodes: { screen: Episodes }
});

export default createBottomTabNavigator(
  {
    Episodes: { screen: EpisodesStack },
    Characters: { screen: CharacterStack }
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let icon;
        if (routeName === "Episodes") {
          icon = require("./images/movie.png");
        } else if (routeName === "Characters") {
          icon = require("./images/character.png");
        }
        return <Image source={icon} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "#25d1f6",
      inactiveTintColor: "gray"
    }
  }
);
