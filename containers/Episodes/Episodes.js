import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, FlatList } from "react-native";
import {
  getAllEpisodes,
  getNextEpisodes
} from "../../redux/actions/apiActions";
import Spinner from "react-native-loading-spinner-overlay";
import EpisodeItem from "../../components/EpisodeItem";

class Episodes extends Component {
  static navigationOptions = {
    title: "Episodes"
  };

  componentDidMount() {
    this.props.getAllEpisodes();
  }

  handleLoadMore = () => {
    const nextEpisodesUrl = this.props.nextEpisodesUrl;
    if (nextEpisodesUrl) this.props.getNextEpisodes(nextEpisodesUrl);
  };

  render() {
    return (
      <View style={style.container}>
        <Spinner
          visible={this.props.getAllEpisodesLoading}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        <FlatList
          keyExtractor={item => item.episode}
          data={this.props.episodes}
          renderItem={({ item }) => <EpisodeItem item={item} />}
          onEndReached={this.handleLoadMore}
          onEndThreshold={0}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    paddingTop: 20
  }
});

function mapStateToProps(state) {
  return {
    episodes: state.rickAndMort.episodes,
    nextEpisodesUrl: state.rickAndMort.nextEpisodesUrl,
    getAllEpisodesLoading: state.rickAndMort.getAllEpisodesLoading
  };
}

export default connect(
  mapStateToProps,
  { getAllEpisodes, getNextEpisodes }
)(Episodes);
