import React, { Component } from "react";
import { connect } from "react-redux";
import { View, StyleSheet } from "react-native";
import { getSelectedCharacterEpisodes } from "../../redux/actions/apiActions";
import Spinner from "react-native-loading-spinner-overlay";
import CharacterDetailItem from "../../components/CharacterDetailItem";
import { findEpisodeIds } from "../../utils";

class CharacterDetail extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      title: navigation.state.params.character.name
    };
  };
  componentDidMount() {
    const { character } = this.props.navigation.state.params;
    const episodeIds = findEpisodeIds(character.episode);
    this.props.getSelectedCharacterEpisodes(episodeIds);
  }

  render() {
    const { character } = this.props.navigation.state.params;

    return (
      <View style={style.container}>
        <Spinner
          visible={this.props.getCharacterEpisodesLoading}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        <CharacterDetailItem
          id="character-detail-item"
          character={character}
          characterEpisodes={this.props.characterEpisodes}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    characterEpisodes: state.rickAndMort.characterEpisodes,
    getCharacterEpisodesLoading: state.rickAndMort.getCharacterEpisodesLoading
  };
}

const style = StyleSheet.create({
  container: { paddingTop: 20 }
});

export default connect(
  mapStateToProps,
  { getSelectedCharacterEpisodes }
)(CharacterDetail);
