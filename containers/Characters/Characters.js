import React, { Component } from "react";
import { connect } from "react-redux";
import { StyleSheet, View, FlatList } from "react-native";
import {
  getAllCharacters,
  getNextCharacters
} from "../../redux/actions/apiActions";
import Spinner from "react-native-loading-spinner-overlay";
import CharacterItem from "../../components/CharacterItem";

class Characters extends Component {
  static navigationOptions = {
    title: "Characters"
  };

  componentDidMount() {
    this.props.getAllCharacters();
  }

  handleLoadMore = () => {
    const nextCharactersUrl = this.props.nextCharactersUrl;
    if (nextCharactersUrl) this.props.getNextCharacters(nextCharactersUrl);
  };

  handleCharacterSelect(character) {
    this.props.navigation.navigate("CharacterDetail", { character: character });
  }

  render() {
    return (
      <View style={style.container}>
        <Spinner
          visible={this.props.getAllCharactersLoading}
          textContent={"Loading..."}
          textStyle={{ color: "#FFF" }}
        />
        <FlatList
          keyExtractor={item => `${item.id}`}
          data={this.props.characters}
          renderItem={({ item }) => (
            <CharacterItem
              item={item}
              handleSelect={() => this.handleCharacterSelect(item)}
            />
          )}
          onEndReached={this.handleLoadMore}
          onEndThreshold={0}
        />
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    backgroundColor: "#eaeaea",
    paddingTop: 20
  }
});

function mapStateToProps(state) {
  return {
    characters: state.rickAndMort.characters,
    nextCharactersUrl: state.rickAndMort.nextCharactersUrl,
    getAllCharactersLoading: state.rickAndMort.getAllCharactersLoading
  };
}

export default connect(
  mapStateToProps,
  { getAllCharacters, getNextCharacters }
)(Characters);
