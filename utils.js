import isEmpty from "lodash/isEmpty";
export const findEpisodeIds = episodes => {
  if (isEmpty(episodes)) return "";
  return episodes
    .slice(Math.max(episodes.length - 5, 0))
    .join("")
    .match(/[0-9]+/g)
    .reverse()
    .join(",");
};
