import { findEpisodeIds } from "../utils";

const emptyArray = [];
const oneEpisodeArray = ["https://rickandmortyapi.com/api/episode/1"];
const twoEpisodeArray = [
  "https://rickandmortyapi.com/api/episode/1",
  "https://rickandmortyapi.com/api/episode/2"
];
const fiveEpisodeArray = [
  "https://rickandmortyapi.com/api/episode/1",
  "https://rickandmortyapi.com/api/episode/2",
  "https://rickandmortyapi.com/api/episode/3",
  "https://rickandmortyapi.com/api/episode/4",
  "https://rickandmortyapi.com/api/episode/5"
];
const sixEpisodeArray = [
  "https://rickandmortyapi.com/api/episode/1",
  "https://rickandmortyapi.com/api/episode/2",
  "https://rickandmortyapi.com/api/episode/3",
  "https://rickandmortyapi.com/api/episode/4",
  "https://rickandmortyapi.com/api/episode/5",
  "https://rickandmortyapi.com/api/episode/6"
];

describe("find episode ids", function() {
  test("should find last 5 episode ids", function() {
    expect(findEpisodeIds(emptyArray)).toEqual("");
    expect(findEpisodeIds(oneEpisodeArray)).toEqual("1");
    expect(findEpisodeIds(twoEpisodeArray)).toEqual("2,1");
    expect(findEpisodeIds(fiveEpisodeArray)).toEqual("5,4,3,2,1");
    expect(findEpisodeIds(sixEpisodeArray)).toEqual("6,5,4,3,2");
  });
});
