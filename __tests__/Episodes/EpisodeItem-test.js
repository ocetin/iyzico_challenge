import "../../setupTest";
import EpisodeItem from "../../components/EpisodeItem";

const episodeItem = {
  air_date: "December 2, 2013",
  characters: (19)[
    ("https://rickandmortyapi.com/api/character/1",
    "https://rickandmortyapi.com/api/character/2",
    "https://rickandmortyapi.com/api/character/35",
    "https://rickandmortyapi.com/api/character/38",
    "https://rickandmortyapi.com/api/character/62",
    "https://rickandmortyapi.com/api/character/92",
    "https://rickandmortyapi.com/api/character/127",
    "https://rickandmortyapi.com/api/character/144",
    "https://rickandmortyapi.com/api/character/158",
    "https://rickandmortyapi.com/api/character/175",
    "https://rickandmortyapi.com/api/character/179",
    "https://rickandmortyapi.com/api/character/181",
    "https://rickandmortyapi.com/api/character/239",
    "https://rickandmortyapi.com/api/character/249",
    "https://rickandmortyapi.com/api/character/271",
    "https://rickandmortyapi.com/api/character/338",
    "https://rickandmortyapi.com/api/character/394",
    "https://rickandmortyapi.com/api/character/395",
    "https://rickandmortyapi.com/api/character/435")
  ],
  created: "2017-11-10T12:56:33.798Z",
  episode: "S01E01",
  id: 1,
  name: "Pilot",
  url: "https://rickandmortyapi.com/api/episode/1"
};

let findById = function(tree, testID) {
  if (tree.props && tree.props.testID === testID) {
    return tree;
  }
  if (tree.children && tree.children.length > 0) {
    let childs = tree.children;
    for (let i = 0; i < childs.length; i++) {
      let item = findById(childs[i], testID);
      if (typeof item !== "undefined") {
        return item;
      }
    }
  }
};

describe("<EpisodeItem />", () => {
  test("episodes item", () => {
    const component = shallow(<EpisodeItem item={episodeItem} />);
    expect(
      component.find("#episode-name").getElements()[0].props.children
    ).toBe(`Name: ${episodeItem.name}`);
    expect(component.find("#air-date").getElements()[0].props.children).toBe(
      `Air date: ${episodeItem.air_date}`
    );
    expect(component.find("#episode").getElements()[0].props.children).toBe(
      `Episode: ${episodeItem.episode}`
    );
    expect(toJson(component)).toMatchSnapshot();
  });
});
