import thunk from "redux-thunk";
import configureStore from "redux-mock-store";
import CharacterDetail from "../../containers/Characters/CharacterDetail";
import "../../setupTest";

describe("<CharacterDetail />", () => {
  const initialState = {
    rickAndMort: {
      characterEpisodes: [
        {
          air_date: "August 27, 2017",
          characters: [
            "https://rickandmortyapi.com/api/character/1",
            "https://rickandmortyapi.com/api/character/2",
            "https://rickandmortyapi.com/api/character/3"
          ],
          created: "2017-11-10T12:56:36.515Z",
          episode: "S03E06",
          id: 27,
          name: "Rest and Ricklaxation",
          url: "https://rickandmortyapi.com/api/episode/27"
        },
        {
          air_date: "August 27, 2017",
          characters: [
            "https://rickandmortyapi.com/api/character/1",
            "https://rickandmortyapi.com/api/character/2",
            "https://rickandmortyapi.com/api/character/3"
          ],
          created: "2017-11-10T12:56:36.515Z",
          episode: "S03E06",
          id: 27,
          name: "Rest and Ricklaxation",
          url: "https://rickandmortyapi.com/api/episode/27"
        }
      ]
    }
  };
  const middleware = [thunk];
  const mockStore = configureStore(middleware);
  const store = mockStore(initialState);
  test("character detail ", () => {
    const wrapper = shallow(
      <CharacterDetail
        store={store}
        navigation={{ state: { params: { character: { episode: [] } } } }}
      />
    );
    const component = wrapper.dive();
    const elements = wrapper.getElements();
    const componentProps = elements[0].props;
    expect(componentProps.characterEpisodes.length).toEqual(2);
    expect(component.find("#character-detail-item").length).toEqual(1);
    expect(toJson(component)).toMatchSnapshot();
  });
});
